package org.crazyit.cloud.isolation;

import java.util.concurrent.ThreadPoolExecutor;

import com.netflix.hystrix.HystrixCircuitBreaker;
import com.netflix.hystrix.HystrixCommand;
import com.netflix.hystrix.HystrixCommandGroupKey;
import com.netflix.hystrix.HystrixCommandProperties;

public class MyCommand extends HystrixCommand<String> {

	int index;

	public MyCommand(int index) {
		super(Setter.withGroupKey(HystrixCommandGroupKey.Factory
				.asKey("ExampleGroup")));
		this.index = index;
	}

	protected String run() throws Exception {
		Thread.sleep(500);
		System.out.println("执行方法，当前索引：" + index);
		return "";
	}

	@Override
	protected String getFallback() {
		System.out.println("执行 fallback，当前索引：" + index);
		return "";
	}
}